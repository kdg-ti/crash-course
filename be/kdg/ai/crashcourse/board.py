import random
from collections import Iterable

from be.kdg.ai.crashcourse.shapes import Shape, Circle, Square


class Board:
    """
    Deze klasse stelt een tekenbord voor waarop enkele shapes geplaatst worden
    """

    # n is the number of shapes
    def __init__(self, width: float, height: float, n: int):
        self.shapes = None
        self._n = n
        self._width = width
        self._height = height
        self.shape_generator = ShapeGenerator(self)

    # deze methode maakt gebruik van een list comprehension en generators
    def fill_board(self) -> None:
        self.shapes = [shape for shape in self.shape_generator]

    @property
    def random_location(self) -> tuple:
        return random.uniform(0, self._width), random.uniform(0, self._height)

    @property
    def width(self) -> float:
        return self._width

    @property
    def height(self) -> float:
        return self._height

    @property
    def n(self):
        return self._n

    def __repr__(self):
        output = ""
        for shape in self.shapes:
            output = output + shape.__repr__()
        return output


class ShapeGenerator(Iterable):
    """
    Deze klasse implementeert een iterator zodat hij gebruikt kan worden als generator

    """

    def __init__(self, board: Board):
        self.board = board
        self.counter = 1
        self.switcher = {
            'Circle': self.create_circle(),
            'Square': self.create_square()
        }

    def __iter__(self):
        return self

    def __next__(self) -> Shape:
        if self.counter > self.board.n:
            raise StopIteration
        shape_class = random.choice(Shape.__subclasses__())
        self.counter = self.counter + 1
        return self.switcher.get(shape_class.__name__)

    def create_circle(self) -> Circle:
        x, y = self.board.random_location
        radius = random.uniform(0, min(self.board.width, self.board.height))
        return Circle(x, y, radius)

    def create_square(self) -> Square:
        x, y = self.board.random_location
        width = random.uniform(0, self.board.width)
        height = random.uniform(0, self.board.height)
        return Square(x, y, width, height)
