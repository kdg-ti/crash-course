from abc import ABC, abstractmethod


class Shape(ABC):
    """
    Shape is een abstract superklasse omdat ze overerft van de klasse ABC
    Deze tekst is commentaar op klasse-niveau
    """

    # Dit is een constructor
    # self heeft dezelfde functie als this in Java maar moet expliciet meegegeven worden
    # x en y zijn argumenten met type hinting (:float)
    def __init__(self, x: float, y: float):
        # in de constructor defineer je alle attributen van een klasse
        self._x = x
        self._y = y

    @abstractmethod
    def draw(self):
        print(self)

    @classmethod
    def generate(cls):
        pass

    # via @property en @x.setter (niet gebruikt hier) kan je 'getters' en 'setters' maken
    # Pycharm heeft daar keyboard-shortcuts voor: 'prop', 'props' en 'propsd'
    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    # Dit is het equivalent van toString in Java, ook mogelijk met __str__ te overriden
    def __repr__(self):
        return "op positie ({:.1f}, {:.1f})".format(self._x, self._y)


class Circle(Shape):
    """
    Circle erft over van Shape en moet dus de methode draw implementeren.
    """

    def __init__(self, x: float, y: float, radius: float):
        super().__init__(x, y)
        self.radius = radius

    def draw(self):
        print(super)
        print(self)

    def __repr__(self):
        return "Cirkel met straal {:.1f} {}\n".format(self.radius, super().__repr__())


class Square(Shape):
    """
    Circle erft over van Shape en moet dus de methode draw implementeren.
    """

    def __init__(self, x: float, y: float, width: float, height: float):
        super().__init__(x, y)
        self.width = width
        self.height = height

    def draw(self):
        print(super)
        print(self)

    def __repr__(self):
        return "Square met hoogte {:.1f} en breedte {:.1f} {}\n".format(self.width, self.height, super().__repr__())
