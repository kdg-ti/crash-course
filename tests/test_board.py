from unittest import TestCase

from be.kdg.ai.crashcourse.board import Board


class TestBoard(TestCase):

    def test_board(self):
        # maak een tekenbord van 10 op 10 met daarop 5 shapes
        board = Board(10, 10, 5)
        # laat het bord zichzelf opvullen met shapes
        board.fill_board()
        # print het bord op het scherm
        print(board)
